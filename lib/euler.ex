defmodule Euler do
  @moduledoc """
  Documentation for Euler.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Euler.hello
      :world

  """

  def solve_number_1(less_than) do
    list = Enum.to_list(1..less_than - 1)
    threes = Enum.filter(list, &(rem(&1, 3) == 0))
    fives = Enum.filter(list, &(rem(&1, 5) == 0))

    Enum.concat(threes, fives)
    |> Enum.uniq
    |> Enum.reduce(&(&1+&2))
  end
end
