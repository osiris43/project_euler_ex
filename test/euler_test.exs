defmodule EulerTest do
  use ExUnit.Case
  doctest Euler

  test "solves number 1" do
    assert Euler.solve_number_1(10) == 23 
  end
end
